\chapter{Konzeption eines Verfahrens zur Optimierung des Ladesäulenmanagements von E-Trucks} \label{k4:konzept}

Dieser Abschnitt soll das Vorgehen bei der Entwicklung eines Algorithmus zur Optimierung des Ladesäulenmanagements von E-Trucks demonstrieren und damit den Tragfähigkeitsnachweis erbringen.\\ 
Zunächst gliedert er sich in die detaillierte Erklärung des Anwendungsszenarios für solch ein Verfahren. Anschließend wird eine formale Beschreibung des dahinter liegenden Problems gezeigt.

\section{Beschreibung des Anwendungsszenarios} \label{k4:szenario}
Ein Lager hat 10 E-Trucks für den Warentransport zur Verfügung. Damit die Fahrzeuge geladen werden können stehen vor dem Lager 5 identische Ladesäulen. Pro Ladesäule kann nur ein E-Truck zur selben Zeit angeschlossen werden. Abbildung \ref{fig:demonstrator_scenario_general} zeigt einen Beispielzustand für dieses Szenario. Hier werden die E-Trucks $T_8$ - $T_{10}$ vor Ort am Lager geladen, während die restlichen sieben Unterwegs sind. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{bilder/szenario/Scenario_General.png}
	\caption{Beispielzustand}
	\label{fig:demonstrator_scenario_general}
\end{figure}

Unterwegs in diesem Kontext heißt, dass der jeweilige E-Truck eine für ihn bestimmte Route abfährt. Die bekannten Informationen über die Route beinhalten wann der E-Truck losfahren muss um rechtzeitig am Ziel anzukommen, wie viel Zeit vergeht bis er wieder am Lager ankommt und die Energie, die die Batterie hierfür benötigt. Pro E-Truck können beliebig viele Routen geplant sein.\\
Nun beenden die E-Trucks $T_7$, $T_6$ und $T_5$, die Unterwegs waren, ihre Route und erreichen nacheinander das Lager. Diese müssen alle für ihre nächsten Routen aufgeladen werden. Die zwei E-Trucks, die zuerst ankommen ($T_7$ und $T_6$) werden an die noch freien Ladesäulen verteilt. Anschließend kommt $T_5$ an, wodurch 6 E-Trucks vor Ort geladen werden müssen, aber nur 5 Ladesäulen vorhanden sind. Diesen Zustand stellt Abbildung \ref{fig:demonstrator_scenario_problem} dar.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{bilder/szenario/Scenario_Problem.png}
	\caption{Problemzustand}
	\label{fig:demonstrator_scenario_problem}
\end{figure}

Nun muss entschieden werden, ob und welcher E-Truck von der jeweiligen Ladesäule getrennt werden müsste damit $T_5$ aufgeladen werden kann. Diese Entscheidung muss anhand der Routendaten der E-Trucks so getroffen werden, dass die Ausnutzung der Ladesäulen maximiert und die Verspätung für die Routen minimiert wird. Dabei darf ein E-Truck nie eine Route befahren, für die er noch nicht entsprechend geladen wurde.\\ \\
Um solche Entscheidungen zu automatisieren ist benötigt man ein System, welches die entsprechenden Daten aufnimmt und einen Zeitplan für alle E-Trucks erstellt. In diesem soll pro Fahrzeug angezeigt werden wann dieses eine Route abfährt und wann er an welcher Ladesäule geladen werden soll. Wie so eine Lösung beispielhaft mit 4 E-Trucks und 2 Ladesäulen aussehen könnte wird in Abbildung \ref{fig:demonstrator_solution_example} demonstriert.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{bilder/szenario/Swimlane_Beispiel.png}
	\caption{Beispiellösung}
	\label{fig:demonstrator_solution_example}
\end{figure}

\section{Zielfunktion und Bedingung} \label{k4:zielfunktionundbedingungen}
Aus dem Szenario in Abschnitt \ref{k4:szenario} lassen sich die Zielfunktion und die Bedingungen dieses Optimierungsproblems ableiten. Zielfunktion in diesem Kontext heißt der Wert der durch die Optimierung minimiert wird. Bedingungen sind die Restriktionen, die bei der Optimierung eingehalten werden müssen. Im folgenden werden diese aufgezeigt.

\textit{Minimiere:}
\begin{itemize}
	\item[(1)] \label{k4:zielfunktiondef} Die Summe der Verspätungen aller Routen im Zeitplan
\end{itemize}
\textit{Nach folgenden Bedingungen:}
\begin{itemize}
	\item[(2)] \label{k4:bedingung1} Für jede Route muss der jeweilige LKW genug geladen werden um diese zu befahren
	\item[(3)] \label{k4:bedingung2} Es können nicht mehr LKWs gleichzeitig geladen werden als die Anzahl von Ladesäulen
	\item[(4)] \label{k4:bedingung3} Ein LKW kann nicht geladen werden während er eine Route abfährt
	\item[(5)] \label{k4:bedingung4} Ein LKW kann vor dem geplanten Abfahrtszeitpunkt der Route nicht losfahren.
\end{itemize}

\section{Formale Beschreibung}
In diesem Abschnitt wird eine formale Beschreibung des Problems gezeigt. Dabei werden zuerst die relevanten Begriffe formal definiert. Anschließend wird mit diesen eine Zielfunktion und Bedingungen für die Optimierung herausgearbeitet.
\subsection*{Begriffsdefinitionen}
Es sei $R$ eine Menge von $i$ Routen, $T$ eine Menge von $j$ E-Trucks.\\

Für alle E-Trucks $t \in T$ sind folgende Größen bekannt:
\begin{itemize}
	\item[-] $q_t$: aktuelle Ladung
	\item[-] $R_t \subseteq R$: eine Menge von Routen
\end{itemize}

Für alle Routen $r_{tn} \in R_t$ sind folgende Größen bekannt:
\begin{itemize}
	\item[-] $z_{r_{tn}}^{^Punkt}$: Zeitpunkt zu dem $t$ losfahren sollte um rechtzeitig anzukommen
	\item[-] $z_{r_{tn}}^{Dauer}$: Zeit die $t$ Unterwegs ist
	\item[-] $q_{r_{tn}}$: Ladung die mindestens benötigt wird um die Strecke fahren zu können.
\end{itemize}

Außerdem sind noch folgende allgemeine Parameter für die Optimierung relevant:
\begin{itemize}
	\item[-] $q^{Zeit}$: Ladung pro Zeiteinheit
	\item[-] $s$: Anzahl Ladesäulen
\end{itemize}

Die Parameter für den Optimierungsalgorithmus setzen sich aus den oben erwähnten Werten zusammen. Durch diese soll der Algorithmus folgende Zahlen berechnen:
\begin{itemize}
	\item[-] $Z^{Loesung}:$ Tupel aus Abfahrtszeitpunkten $z_{r_{tn}}^{Loesung}$ für alle $r_{tn} \in R$, $z_{r_{tn}}^{Loesung} \in \mathbb{N}$
	\item[-] $L_t^{Loesung}:$ Tupel aus Ladezeitpunkten $l_{tn}$ für alle Zeiteinheiten aller LKWs, $l_{tn} \in \{0,1\}, 0 := t$ wird nicht geladen, $1 := t$ wird geladen
	\item[-] $l^{Anzahl}:$ Anzahl der Elemente des $L_t^{Loesung}$-Tupels mit der größten Elementanzahl
\end{itemize}	 


\subsection*{Zielfunktion und Bedingungen}
Mit den obigen Definitionen können wir die Zielfunktion und Bedingungen aufstellen, die im Abschnitt \ref{k4:zielfunktionundbedingungen} beschrieben wurden. Diese werden im folgenden aufgelistet.

\textit{Minimiere:}
\begin{itemize}
	\item[(1)] \label{k4:fzielfunktiondef} $\sum_{m=0}^{i}|z_{m_t}^{punkt}-z_{m_t}^{Loesung}| $
\end{itemize}
\textit{Nach folgenden Bedingungen:}
\begin{itemize}
	\item[(2)] \label{k4:fbedingung1}$\forall r_{tn} \in R: q_t + \sum_{m=0}^{n}(l_{tm}) * q^{Zeit} \geq \sum_{m=0}^{n}z_{r_{tm}}^{Dauer}$
	\item[(3)] \label{k4:fbedingung2}$\forall n \in \{0, 1, ..., l^{Anzahl}\}:\sum_{m=0}^{|T|}l_{mn} \leq s$
	\item[(4)] \label{k4:fbedingung3}$\forall r_{tn} \in R: \sum_{m=z_{r_{tn}}^{^Punkt}}^{z_{r_{tn}}^{Punkt}+z_{r_{tn}}^{Dauer}}l_{tm}=0$
	\item[(5)] \label{k4:fbedingung4}$\forall r_{tn} \in R:z_{r_{tn}}^{Loesung} \geq z_{r_{tn}}^{Punkt}$
\end{itemize}

