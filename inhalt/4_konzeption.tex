\chapter{Konzeption eines Verfahrens zur Optimierung des Ladesäulenmanagements von E-Trucks} \label{k4:konzept}

Dieser Abschnitt soll das Vorgehen bei der Entwicklung eines Algorithmus zur Optimierung des Ladesäulenmanagements von E-Trucks demonstrieren und damit den Tragfähigkeitsnachweis erbringen.\\ 
Zunächst gliedert er sich in die Beschreibung des nichtlineares Optimierungsmodells. Anschließend wird die Methode der Optimierung erklärt. Zuletzt wird der Algorithmus zur Bestimmung des benötigten Startwerts dargestellt. 


\section{Methode der Optimierung: Sequential Quadratic Programming} \label{k4.1:methode}
In diesem Abschnitt wird die Methode beschrieben, die für die Optimierung genutzt wird. Diese muss in der Lage sein, ein nichtlineares Problem durch Näherung zu optimieren. Die Methode, die in dieser Arbeit genutzt wird, heißt Sequential Quadratic Programming. Im Folgenden wird anhand von Krafts Forschungsbericht \cite{kraft1988sequential} das Prinzip dieser Methode näher erläutert. \\ \\
Die Methode benötigt ein nichtlineares Modell und einen Startwert $x^0$, welcher sich im Lösungsraum des Modells befindet und dessen Bedingungen erfüllt. Mit diesem Startwert wird iterativ eine Näherung zu einem optimierten Wert gesucht. Dies geschieht durch folgende Formel:
$$x^{k+1}:=x^k+\alpha^k+d^k$$
Dabei ist $\alpha^k$ die Schrittgröße und $d^k$ die Suchrichtung des Schritts $k$. Die Suchrichtung wird für jeden Schritt als ein Quadratic Programming Subproblem neu berechnet. Die Erklärung dieser Rechnung würde den Rahmen dieser Bachelorarbeit überschreiten und wird deshalb hier nicht näher behandelt. Die Schrittgrößen werden außerhalb der Methode bestimmt und als Parameter übergeben. Dieser Schritt zur Näherung wird so lange ausgeführt, bis die Differenz zwischen $x^k$ und $x^k+1$ kleiner ist als ein zuvor bestimmter Toleranzwert $ACC$, die maximale Anzahl an Iterationen $ITR$ erreicht wird oder wenn ein Problem beim Berechnen entsteht. Im ersten Fall wird erfolgreich ein optimierter Wert erreicht, während in den anderen Fällen die Iteration abgebrochen wird und die Methode fehlschlägt. \\
\cite{kraft1988sequential}
 \\ \\
Also muss folgendes bestimmt werden, um Sequential Quadratic Programming anzuwenden:
\begin{itemize}
	\item [-] Nichtlineares Optimierungsmodell für das Szenario aus Kapitel \ref{k2:problemstellung} 
	\item [-] Verfahren zur Bestimmung eines Startwerts $x^0$
	\item [-] Wert für Schrittgrößen $\alpha^k$ 
	\item [-] Wert für den Toleranzwert $ACC$
	\item [-] Wert für die Anzahl der Iterationen $ITR$
\end{itemize}
Abschnitt \ref{k4.2:optimierungsmodell} beschreibt das nichtlineare Optimierungsmodell, während in Abschnitt \ref{k4.3:startwert} das Verfahren zur Bestimmung eines Startwerts beschrieben wird. Die Abhängigkeit des Optimierungsergebnisses abhängig von den Werten $\alpha^k$, $ACC$ und $ITR$ wird in  Kapitel \ref{k6:evaluation} evaluiert.



\section{Nichtlineares Optimierungsmodell} \label{k4.2:optimierungsmodell}
In diesem Abschnitt wird das nichtlineare Optimierungsmodell für die Problemstellung aus Kapitel \ref{k2:problemstellung} beschrieben. Dabei werden zuerst die dafür notwendigen Begriffe formal definiert. Anschließend werden mit diesen die Zielfunktion und Bedingungen, die aus der Problemstellung abzuleiten sind, modelliert.

\subsection{Begriffsdefinitionen} \label{k4.2.1:definition}
Für die Modellierung wird zunächst eine Menge $T$ von E-Trucks benötigt. Für alle E-Trucks $t_n \in T$ wird folgendes benötigt:
\begin{itemize}
	\item [-] $q_n \in \mathbb{N}$: Den Startwert der Ladung vom jeweiligen E-Truck
	\item [-] $R_n \subseteq R$: Eine Menge der Routen vom jeweiligen E-Truck
\end{itemize}

Für alle Routen $r_{tn} \in R_t$ sind folgende Größen bekannt:
\begin{itemize}
	\item[-] $z_{tn}^{Punkt}\in \mathbb{N}$: Zeitpunkt, zu dem $t$ losfahren sollte, um rechtzeitig anzukommen
	\item[-] $z_{tn}^{Dauer}\in \mathbb{N}$: Zeit, die $t$ unterwegs ist
	\item[-] $q_{tn}\in \mathbb{N}$: Ladung, die mindestens benötigt wird, um die Strecke fahren zu können.
\end{itemize}

Außerdem sind noch folgende allgemeine Parameter für die Optimierung relevant:
\begin{itemize}
	\item[-] $q^{Zeit}\in \mathbb{N}$: Ladung pro Zeitspanne der Ladesäulen
	\item[-] $s\in \mathbb{N}$: Anzahl der Ladesäulen
	\item[-] $z^{Anzahl}\in \mathbb{N}$: Anzahl der zu beachtenden Zeitspannen
\end{itemize}

Die Parameter für den Optimierungsalgorithmus setzen sich aus den oben erwähnten Werten zusammen. Durch diese soll der Algorithmus die Daten berechnen, die benötigt werden, um einen Zeitplan zu erstellen.
\begin{itemize}
	\item[-] $Z^{Loesung}:$ Tupel aus Abfahrtszeitpunkten $z_{tn}^{Loesung}$ für alle $r_{tn} \in R$, $z_{tn}^{Loesung} \in \mathbb{N}$
	\item[-] $L_t^{Loesung}:$ Tupel aus $z^{Anzahl}$ Ladezeitspannen $l_{tn}$ für alle E-Trucks, $l_{tn} \in \{0,1\}, 0 := t$ wird nicht geladen, $1 := t$ wird geladen
\end{itemize}	 


\subsection{Zielfunktion und Bedingungen} \label{k4.2.2:zielfunktion}
Mit den obigen Definitionen können die Zielfunktion und die Bedingungen der Optimierung aufgestellt werden. Aus der Problemstellung in Kapitel \ref{k2:problemstellung} lassen sich diese ableiten. Sie lauten wie folgt: \\ \\
\textit{Zielfunktion:}
\begin{itemize}
	\item[] \label{k4:zielfunktiondef} Minimiere die Summe der Verspätung aller Routen jedes E-Trucks.
\end{itemize}
\textit{Bedingungen:}
\begin{itemize}
	\item[(B1)] \label{k4:bedingung1} Für jede Route muss der jeweilige E-Truck genug geladen werden, um diese zu befahren.
	\item[(B2)] \label{k4:bedingung2} Es können nicht mehr E-Truck gleichzeitig geladen werden als die gegebene Anzahl der Ladesäulen.
	\item[(B3)] \label{k4:bedingung3} Ein E-Truck kann nicht geladen werden, während er eine Route abfährt.
	\item[(B4)] \label{k4:bedingung4} Ein E-Truck kann vor dem geplanten Abfahrtszeitpunkt der Route nicht losfahren.
	\item[(B5)] \label{k4:bedingung5} Ein E-Truck kann keine Route befahren, während er eine andere Route befährt.
\end{itemize}

Nun wird im Folgenden aus diesen sechs Punkten schrittweise das Optimierungsmodell erstellt: \\ \\
\subsubsection*{Zielfunktion:}
 Um die Zielfunktion zu berechnen, muss für jeden E-Truck die Verspätung jeder Route $r_{tn}$  ermittelt werden. Diese kann man berechnen, indem der geplante Abfahrtszeitpunkt $z_{tn}^{Punkt}$ vom tatsächlichen Abfahrtszeitpunkt $z_{tn}^{Loesung}$ subtrahiert wird. Anschließend müssen die Verspätungen aller Routen summiert werden. Also lautet die Zielfunktion:

 $$Minimiere:\sum_{n=0}^{|T|}\sum_{m=0}^{|R_n|}|(z_{nm}^{Loesung}-z_{nm}^{Punkt})|$$

\subsubsection*{Bedingung B1:}
Für diese Bedingung muss für jede Route $r_{tn} \in R$ geprüft werden, ob die Ladung des E-Trucks zum tatsächlichen Abfahrtszeitpunkt $z_{tn}^{Loesung}$ ausreicht, um diese zu befahren. Dabei darf diese Ladung nicht kleiner sein als die benötige Ladung $q_{tn}$. Um dies zu berechnen, muss bekannt sein, wie viel Ladung der E-Truck zu diesem Zeitpunkt besitzt. Dieser Wert kann berechnet werden, indem der Startwert der Ladung $q_t$ des jeweiligen E-Trucks zu der Ladung, die dieser durch die Ladesäulen erhalten hat, hinzu addiert wird und die Summe der Ladungskosten aller Routen, die der E-Truck bis zu diesem Zeitpunkt befahren hat, subtrahiert wird. Um die Ladung zu berechnen, die der E-Truck bis zu diesem Zeitpunkt von den Ladesäulen erhalten hat, werden alle $l_{tm}$ bis zum Abfahrtszeitpunkt $z_{tn}^{Loesung}$ summiert und mit der Ladung pro Zeitspanne $q^{Zeit}$ der Ladesäulen multipliziert. Daraus ergibt sich:

$$\forall r_{tn} \in R: q_t + \sum_{m=0}^{z_{tn}^{Loesung}}(l_{tm}) * q^{Zeit} - \sum_{m=0}^{n-1}q_{tm} \geq q_{tn}$$

\subsubsection*{Bedingung B2:}
Für diese Bedingung muss für jede Zeitspanne $n \in \{0, 1, ... z^{Anzahl} - 1\}$ geprüft werden, ob zu dieser Zeit nicht mehr E-Trucks geladen werden als die Anzahl $s$ der Ladesäulen, die zur Verfügung stehen. Daraus ergibt sich:

$$\forall n \in \{0, 1, ..., z^{Anzahl} - 1\}:\sum_{m=0}^{|T|}l_{mn} \leq s$$

\subsubsection*{Bedingung B3:}
Für diese Bedingung muss für jede Route $r_{tn}$ geprüft werden, ob die Werte der Ladezeitpunkte in $L_{t}^{Loesung}$ des jeweiligen E-Trucks $0$ betragen, solange dieser für die jeweilige Route unterwegs ist, also ab dem Abfahrtszeitpunkt $z_{tn}^{Punkt}$ bis zur Wiederankunft zum Zeitpunkt $z_{tn}^{Punkt}+z_{tn}^{Dauer}$. Daraus ergibt sich:

$$\forall r_{tn} \in R: \sum_{m=z_{tn}^{Punkt}}^{z_{tn}^{Punkt}+z_{tn}^{Dauer}}l_{tm}=0$$


\subsubsection*{Bedingung B4:}
Für diese Bedingung muss für jede Route $r_{tn}$ geprüft werden, ob der geplante Abfahrtszeitpunkt $z_{tn}^{Punkt}$ nicht nach dem tatsächlichen Abfahrtszeitpunkt $z_{tn}^{Punkt}$ stattfindet. Daraus ergibt sich:

$$\forall r_{tn} \in R:z_{tn}^{Loesung} \geq z_{tn}^{Punkt}$$

\subsubsection*{Bedingung B5:}
Für diese Bedingung muss für jede Route $r_{tn}$ geprüft werden, ob der jeweilige E-Truck diese abgeschlossen hat, bevor der tatsächliche Abfahrtszeitpunkt $z_{tn}^{Loesung}$ der nächsten Route stattfindet. Diese Bedingung gilt nicht für die letzte Route des jeweiligen E-Trucks, da danach keine weitere Route folgen kann. Daraus ergibt sich:

$$\forall r_{tn} \in R \setminus \{r_{0(|R_0|-1)}, r_{1(|R_1|-1)}, ..., r_{(|T|-1|)(R_{|T|-1}|-1)}\}: z_{tn}^{Loesung}+z_{tn}^{Dauer} \leq z_{t(n+1)}^{Loesung} $$

Also sieht das nichtlineare Optimierungsmodell folgendermaßen aus:

\textit{Zielfunktion:}
 $$Minimiere:\sum_{n=0}^{|T|}\sum_{m=0}^{|R_n|}|(z_{nm}^{Loesung}-z_{nm}^{Punkt})|$$\\
\textit{Bedingungen:}
$$(B1):\forall r_{tn} \in R: q_t + \sum_{m=0}^{z_{tn}^{Loesung}}(l_{tm}) * q^{Zeit} - \sum_{m=0}^{n-1}q_{tm} \geq q_{tn}$$
$$(B2):\forall n \in \{0, 1, ..., z^{Anzahl} - 1\}:\sum_{m=0}^{|T|}l_{mn} \leq s$$
$$(B3):\forall r_{tn} \in R: \sum_{m=z_{tn}^{Punkt}}^{z_{tn}^{Punkt}+z_{tn}^{Dauer}}l_{tm}=0$$
$$(B4):\forall r_{tn} \in R:z_{tn}^{Loesung} \geq z_{tn}^{Punkt}$$
$$(B5):\forall r_{tn} \in R \setminus \{r_{0(|R_0|-1)}, r_{1(|R_1|-1)}, ..., r_{(|T|-1|)(R_{|T|-1}|-1)}\}: z_{tn}^{Loesung}+z_{tn}^{Dauer} \leq z_{t(n+1)}^{Loesung} $$

\section{Algorithmus zur Bestimmung eines Startwerts} \label{k4.3:startwert}
In diesem Abschnitt wird beschrieben, wie ein Startwert im Lösungsraum des Modells im Abschnitt \ref{k4.2:optimierungsmodell} automatisiert gefunden werden kann. Dabei ist es wichtig, dass alle dazugehörigen Bedingungen eingehalten werden. Dieser Algorithmus wird später genutzt, um den benötigten Startwert $x^0$ der Methode Sequential Quadratic Programming (siehe Abschnitt \ref{k4.1:methode}) zu finden. \\ \\
Im Folgenden werden zuerst die Eingabeparameter und Ausgabewerte des Algorithmus aufgezählt. Anschließend wird der Vorgang des Algorithmus als Pseudocode vorgestellt und  beschrieben.
\subsection{Eingabeparameter und Ausgabewerte}
Um einen Startwert für das Modell zu finden, muss für jede Route jedes E-Trucks eine Abfahrtszeit bestimmt werden. Außerdem muss für jede Zeitspanne der E-Trucks bestimmt werden, ob dieser aufgeladen wird oder nicht. Da bis hier die Anzahl der Zeitspannen unbekannt ist, müssen diese auch bestimmt werden. Die Eingabeparameter und Ausgabewerte, die hierfür benötigt werden, werden im Abschnitt \ref{k4.2.1:definition} definiert und lauten wie folgt: \\ \\
\textit{Eingabeparameter:}
\begin{itemize}
	\item[-] $T$: Menge aller E-Trucks
	\item[-] $R_n$: Menge aller Routen des E-Trucks $t_n$ 
	\item[-] $q^{Zeit}$: Ladung pro Zeitspanne der Ladesäulen
	\item[-] $s$: Anzahl der Ladesäulen
\end{itemize}
\textit{Ausgabewerte:}
\begin{itemize}
	\item[-] $Z^{Loesung}:$ Tupel aus Abfahrtszeitpunkten
	\item[-] $z^{Anzahl}$: Anzahl der zu beachtenden Zeitspannen
	\item[-] $L_n^{Loesung}:$ Tupel aus $z^{Anzahl}$ Ladezeitspannen $l_{nm}$ des E-Trucks $t_n$
\end{itemize}

\subsection{Pseudocode}
Die Implementierung dieses Algorithmus könnte folgendermaßen aussehen: \\ \\
\begin{algorithm}[H]
	$Z^{Loesung}=$ [ ] \\
	\ForAll{$t_n$ in $T$} {
		$L_n^{Loesung}=$ [ ]\\
		\ForAll{$r_{nm}$ in $R_n$} {
			$z^{Aktuell}=$ Anzahl der Elemente in $L_n^{Loesung}$\\
			$q^{Aktuell}=$ Ladung zum Zeitpunkt $z^{Aktuell}$\\
			$q^{diff}=q^{Aktuell}-q_{nm}$\\
			\If{$q^{diff} < 0$}{
				$l^{Anzahl}=$ aufgerundet$(|q^{diff}/q^{Zeit}|)$ \\
				\For{$i=0;i<l^{Anzahl};i$++} {
					\While{true} {
						$z^{Aktuell}=$ Anzahl der Elemente in $L_n^{Loesung}$\\
						\eIf{(Anzahl E-Trucks die zum Zeitpunkt $z^{Aktuell}$ geladen werden) < s}{
							Füge eine 1 in $L_n^{Loesung}$ hinzu \\
							break\\
						}{
							Füge eine 0 in $L_n^{Loesung}$ hinzu \\
						}
					}
				}
				$z^{Aktuell}=$ Anzahl der Elemente in $L_n^{Loesung}$\\
			}
			$z^{Diff}=z^{Punkt}_{nm}-z^{Aktuell}$\\
			\If{$z^{Diff}>0$} {
				Füge $z^{Diff}$ mal eine $0$ in $L_n^{Loesung}$ hinzu \\
				$z^{Aktuell}=$ Anzahl der Elemente in $L_n^{Loesung}$\\
			}
			Füge $z^{Aktuell}$ in $Z^{Loesung}$ hinzu\\
			Füge $z^{Dauer}_{nm}$ mal eine $0$ in $L_n^{Loesung}$ hinzu \\
		}
	}
	$z^{Anzahl}$ = Ermittle die Länge der längsten Ladezeitspannenliste $L^{Loesung}_x$ \\
	Fülle alle Ladezeitspannenlisten mit Nullen bis sie die Länge $z^{Anzahl}$ erreichen
	
\end{algorithm}
\subsection{Erklärung des Verfahrens}
Dieser Abschnitt beschreibt das Vorgehen zur Bestimmung eines Startwerts, welcher die Bedingungen des Optimierungsmodells erfüllt. \\ \\
Zuerst wird nacheinander durch alle Routen jedes E-Trucks iteriert. In jeder Iteration wird zunächst die aktuelle Zeitspanne $z^{Aktuell}$ anhand der Länge der Liste $L_n^{Loesung}$ ermittelt. Anschließend wird die Ladung $q^{Aktuell}$ des E-Trucks zu dieser Zeitspanne berechnet. Daraufhin wird die Differenz $q^{diff}$ zwischen der aktuellen Ladung und der benötigten Ladung für diese Route berechnet. Falls dies kleiner Null ist, heißt das, dass nicht genügend Ladung zur Verfügung steht, um diese Route zu befahren. Also wird zunächst die Anzahl der Ladezeitspannen $l^{Anzahl}$ berechnet, die nötig sind, um die Route befahren zu können. Darauf folgend wird für jede benötigte Ladezeitspanne eine Zeitspanne gesucht, in der noch nicht alle Ladesäulen beansprucht sind. Wenn eine gefunden wurde, wird eine Eins in die Liste der Ladezeitspannen $L_n^{Loesung}$ hinzugefügt. Ansonsten wird eine Null eingetragen und es wird weiter gesucht. Anschließend wird erneut $z^{Aktuell}$ anhand der Länge der Liste $L_n^{Loesung}$ berechnet. Nun wird die Differenz $z^{Diff}$ zwischen dem geplanten Abfahrtszeitpunkt $z^{Punkt}_{nm}$ und der aktuellen Zeitspanne $z^{Aktuell}$ ermittelt. Falls dieser Wert größer Null ist, bedeutet das, dass mit der Abfahrt noch gewartet werden muss, da der Abfahrtszeitpunkt des E-Trucks nicht vor der geplanten Zeit sein darf. Also wird in die Liste der Ladezeitspannen $z^{Diff}$ mal eine Null hinzugefügt, da der E-Truck nun in einem Wartezustand ist. Anschließend muss erneut der aktuelle Zeitpunkt $z^{Aktuell}$ neu berechnet werden. Nun kann dieser als tatsächlicher Abfahrtszeitpunkt für diese Route in $Z^{Loesung}$ eingetragen werden. Zum Schluss muss die Ladezeitspanne mit so vielen Nullen gefüllt werden, wie die Dauer der jeweiligen Route, da der E-Truck zu diesem Zeitpunkt nicht geladen werden kann. Sobald dies für alle Routen jedes E-Trucks gemacht wurde, werden die Liste der Ladezeitspannen auf die selbe Länge $z^{Anzahl}$ gebracht. Dieser Wert wurde zuvor durch die Länge der längsten Liste der Ladezeitspannen ermittelt.
